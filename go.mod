module gitlab.com/l2jeloquens/l2jeloquens-api

go 1.17

require (
	github.com/gofiber/fiber/v2 v2.24.0
	github.com/gofiber/storage/memory v0.0.0-20211117053443-4a3096149ebb
	github.com/joho/godotenv v1.4.0
	github.com/morkid/paginate v1.1.4
	gorm.io/driver/mysql v1.2.2
	gorm.io/gorm v1.22.4
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator/v10 v10.10.0 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/iancoleman/strcase v0.2.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/morkid/gocache v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.31.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.6 // indirect
)
