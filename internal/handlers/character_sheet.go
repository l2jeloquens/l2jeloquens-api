package handlers

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/morkid/paginate"
	"gitlab.com/l2jeloquens/l2jeloquens-api/internal/database"
	"gitlab.com/l2jeloquens/l2jeloquens-api/internal/model"
)

func GetCharacterSheets(c *fiber.Ctx) error {
	var sheets []model.CharacterSheet
	var data interface{}
	var err *fiber.Error

	db := database.DB
	isErr := true
	data = nil

	model := db.Model(&sheets)
	pg := paginate.New()
	page := pg.With(model).Request(c.Request()).Response(&sheets)

	if page.Total > 0 {
		err = fiber.NewError(fiber.StatusOK)
		isErr = false
		data = page
	} else {
		err = fiber.ErrNotFound
	}

	return c.Status(err.Code).JSON(fiber.Map{
		"error":   isErr,
		"data":    data,
		"message": err.Message})
}

func GetCharacterSheet(c *fiber.Ctx) error {
	var sheet model.CharacterSheet
	var data interface{}
	var err *fiber.Error

	db := database.DB
	isErr := true
	data = nil

	charId := c.Params("char_id")
	db.Find(&sheet, "charId = ?", charId)

	if sheet.CharID > 0 {
		err = fiber.NewError(fiber.StatusOK)
		isErr = false
		data = sheet
	} else {
		err = fiber.ErrNotFound
	}

	return c.Status(err.Code).JSON(fiber.Map{
		"error":   isErr,
		"data":    data,
		"message": err.Message,
	})
}

func UpdateCharacterSheet(c *fiber.Ctx) error {
	var sheet model.CharacterSheet
	var data interface{}
	var err *fiber.Error

	db := database.DB
	isErr := true
	data = nil

	// Find record in DB
	charId := c.Params("char_id")
	db.Find(&sheet, "charId = ?", charId)

	// Check if record exists
	if sheet.CharID <= 0 {
		err = fiber.ErrNotFound
	}

	// Check if incoming payload is processable
	if err == nil {
		parseErr := c.BodyParser(&sheet)
		if parseErr != nil {
			err = fiber.ErrBadRequest
			err.Message = parseErr.Error()
		}
	}

	// Validate user input
	if err == nil {
		validate := validator.New()
		valErr := validate.Struct(&sheet)
		if valErr != nil {
			err = fiber.ErrUnprocessableEntity
			err.Message = valErr.Error()
		}
	}

	// Update the record and check if there is any error
	if err == nil {
		upd := db.Save(&sheet)
		if upd.Error != nil {
			err = fiber.ErrInternalServerError
		} else {
			err = fiber.NewError(fiber.StatusOK)
			isErr = false
			data = sheet
		}
	}

	// Handler response
	return c.Status(err.Code).JSON(fiber.Map{
		"error":   isErr,
		"data":    data,
		"message": err.Message,
	})
}

func DeleteCharacterSheet(c *fiber.Ctx) error {
	var sheet model.CharacterSheet
	var data interface{}
	var err *fiber.Error

	db := database.DB
	isErr := true
	data = nil

	charId := c.Params("char_id")
	db.Find(&sheet, "charId = ?", charId)

	// Check if record exists
	if sheet.CharID <= 0 {
		err = fiber.ErrNotFound
	}

	// Delate the record and check if there is any error
	if err == nil {
		del := db.Delete(&sheet, "charId = ?", charId)

		if del.Error != nil {
			err = fiber.ErrInternalServerError
		} else {
			err = fiber.NewError(fiber.StatusOK)
			isErr = false
		}
	}

	// Handler response
	return c.Status(err.Code).JSON(fiber.Map{
		"error":   isErr,
		"data":    data,
		"message": err.Message,
	})
}
