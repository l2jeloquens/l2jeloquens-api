package middleware

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"github.com/gofiber/storage/memory"
	"gitlab.com/l2jeloquens/l2jeloquens-api/configs"
)

func SetupRateLimiter(app *fiber.App, limiterStorage *memory.Storage) {
	cfg := configs.CFG
	app.Use(limiter.New(limiter.Config{
		Max:        cfg.LimMaxRequests,
		Expiration: time.Duration(cfg.LimExpTime) * time.Second,
		LimitReached: func(c *fiber.Ctx) error {
			return c.SendStatus(cfg.LimErrCode)
		},
		SkipFailedRequests:     cfg.LimSkipFailedReq,
		SkipSuccessfulRequests: cfg.LimSkipSuccessReq,
		Storage:                limiterStorage,
	}))
}
