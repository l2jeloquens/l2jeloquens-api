package middleware

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"gitlab.com/l2jeloquens/l2jeloquens-api/pkg/filesystem"
)

type RequestFormat struct {
	RequestID string `json:"request_id"`
	Time      string `json:"time"`
	Origin    string `json:"origin"`
	Method    string `json:"method"`
	Status    string `json:"status"`
	Latency   string `json:"latency"`
	Path      string `json:"path"`
}

func SetupLogger(app *fiber.App) {
	logFile := filesystem.OpenFile("./logs/requests.log")

	reqFormat := RequestFormat{
		RequestID: "${locals:requestid}",
		Time:      "${time}",
		Origin:    "${ip}:${port}",
		Method:    "${ip}:${port}",
		Status:    "${status}",
		Latency:   "${latency}",
		Path:      "${path}",
	}

	format, err := json.Marshal(reqFormat)

	if err != nil {
		log.Printf("[LoggerMiddleware] Error parsing request format: %v\n", err)
		format = []byte("[${time}] ${status} - ${latency} ${method} ${path}")
	}

	app.Use(logger.New(logger.Config{
		Next:         nil,
		Format:       fmt.Sprintf("%s\n", format),
		TimeFormat:   "15:04:05",
		TimeZone:     "Local",
		TimeInterval: 500 * time.Millisecond,
		Output:       logFile,
	}))
}
