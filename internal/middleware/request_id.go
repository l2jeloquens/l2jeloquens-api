package middleware

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

func SetupRequestId(app *fiber.App) {
	app.Use(requestid.New())
}
