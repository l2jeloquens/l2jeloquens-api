package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func SetupApiRoutes(app *fiber.App) {
	api := app.Group("/api", logger.New())

	// Setup routes, can use same syntax to add routes for more models
	setupCharacterSheetsRoutes(api)
}
