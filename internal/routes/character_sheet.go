package routes

import (
	"github.com/gofiber/fiber/v2"
	h "gitlab.com/l2jeloquens/l2jeloquens-api/internal/handlers"
)

func setupCharacterSheetsRoutes(router fiber.Router) {
	note := router.Group("/sheets")

	// Retrieve all character sheets
	note.Get("/", h.GetCharacterSheets)
	note.Get("/:char_id", h.GetCharacterSheet)
	note.Patch("/:char_id", h.UpdateCharacterSheet)
	note.Delete("/:char_id", h.DeleteCharacterSheet)
}
