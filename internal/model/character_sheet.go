package model

type CharacterSheet struct {
	CharID         int    `gorm:"column:charId;primaryKey" json:"char_id"`
	Active         int    `gorm:"column:active" validate:"gte=0,lte=1" json:"active"`
	Name           string `gorm:"column:name" validate:"min=3,max=30" json:"name"`
	Nickname       string `gorm:"column:nickname" json:"nickname"`
	Race           string `gorm:"column:race" json:"race"`
	Age            string `gorm:"column:age" json:"age"`
	Occupation     string `gorm:"column:occupation" json:"occupation"`
	Description    string `gorm:"column:description" json:"description"`
	Background     string `gorm:"column:background" json:"background"`
	DiscordAccount string `gorm:"column:discord_account" json:"discord_account"`
	RpLines        string `gorm:"column:rp_lines" json:"rp_lines"`
	RpVeils        string `gorm:"column:rp_veils" json:"rp_veils"`
}
