package configs

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
	p "gitlab.com/l2jeloquens/l2jeloquens-api/pkg/parsers"
)

type Config struct {
	APPName           string
	APPVersion        string
	Prefork           bool
	CaseSensitive     bool
	StrictRouting     bool
	DBHost            string
	DBName            string
	DBUser            string
	DBPassword        string
	DBPort            int
	Limiter           bool
	LimMaxRequests    int
	LimExpTime        int // seconds
	LimSkipFailedReq  bool
	LimSkipSuccessReq bool
	LimErrCode        int
	Logger            bool
}

var CFG *Config

func Load() {
	err := godotenv.Load("config.ini")

	if err != nil {
		fmt.Println("Error loading config file")
	}

	CFG = &Config{
		// App
		APPName:       p.GetString("APP_NAME", "L2J API"),
		APPVersion:    p.GetString("APP_VERSION", "1.0.0"),
		Prefork:       p.GetBool("PREFORK_ENABLED", false),
		CaseSensitive: p.GetBool("CASE_SENSITIVE", false),
		StrictRouting: p.GetBool("STRICT_ROUTING", false),

		// Database
		DBHost:     p.GetString("DB_HOST", "127.0.0.1"),
		DBName:     p.GetString("DB_NAME", "l2jserver"),
		DBUser:     p.GetString("DB_USER", "root"),
		DBPassword: p.GetString("DB_PASSWORD", ""),
		DBPort:     p.GetInt("DB_PORT", 3306),

		// Rate limiter
		Limiter:           p.GetBool("LIM_ENABLED", false),
		LimMaxRequests:    p.GetInt("LIM_MAX_REQUESTS", 5),
		LimExpTime:        p.GetInt("LIM_EXP_TIME", 60),
		LimSkipFailedReq:  p.GetBool("LIM_SKIP_FAILED_REQ", false),
		LimSkipSuccessReq: p.GetBool("LIM_SKIP_SUCCESS_REQ", false),
		LimErrCode:        p.GetInt("LIM_ERR_CODE", fiber.StatusTooManyRequests),

		// Logger
		Logger: p.GetBool("LOGGER_ENABLED", false),
	}
}
