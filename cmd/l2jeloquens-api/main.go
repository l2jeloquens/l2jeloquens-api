package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/etag"
	"github.com/gofiber/storage/memory"
	"gitlab.com/l2jeloquens/l2jeloquens-api/configs"
	"gitlab.com/l2jeloquens/l2jeloquens-api/internal/database"
	"gitlab.com/l2jeloquens/l2jeloquens-api/internal/middleware"
	"gitlab.com/l2jeloquens/l2jeloquens-api/internal/routes"
)

func main() {

	configs.Load()
	cfg := configs.CFG

	// Fiber APP INIT
	app := fiber.New(fiber.Config{
		AppName: fmt.Sprintf("%s v%s",
			cfg.APPName,
			cfg.APPVersion),
		Prefork:       cfg.Prefork,
		CaseSensitive: cfg.CaseSensitive,
		StrictRouting: cfg.StrictRouting,
	})

	// Load middlewares
	middleware.SetupRequestId(app)

	app.Use(etag.New(etag.Config{
		Next: nil,
		Weak: false,
	}))

	if cfg.Limiter {
		memStorage := memory.New()
		middleware.SetupRateLimiter(app, memStorage)
	}

	if cfg.Logger {
		_ = os.Mkdir("./logs", os.ModePerm)
		middleware.SetupLogger(app)
	}

	// Connect to the Database
	database.ConnectDB()

	// Load router
	routes.SetupApiRoutes(app)

	log.Fatal(app.Listen(":3000"))
}
