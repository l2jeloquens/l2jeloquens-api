package parsers

import (
	"os"
	"strconv"
)

func GetString(key string, def string) (value string) {
	rawValue := os.Getenv(key)

	if rawValue == "" {
		value = def
	} else {
		value = rawValue
	}

	return value
}

func GetInt(key string, def int) (value int) {
	rawValue := os.Getenv(key)

	if rawValue == "" {
		value = def
	} else {
		if intVal, err := strconv.ParseInt(rawValue, 10, 32); err != nil {
			value = def
		} else {
			value = int(intVal)
		}
	}

	return value
}

func GetBool(key string, def bool) (value bool) {
	rawValue := os.Getenv(key)

	if rawValue == "" {
		value = def
	} else {
		if boolVar, err := strconv.ParseBool(rawValue); err != nil {
			value = def
		} else {
			value = boolVar
		}
	}

	return value
}
