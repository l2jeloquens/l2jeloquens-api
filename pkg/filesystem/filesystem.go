package filesystem

import (
	"log"
	"os"
)

func OpenFile(path string) *os.File {
	file, err := os.OpenFile(
		path,
		os.O_RDWR|os.O_CREATE|os.O_APPEND,
		0666)
	if err != nil {
		log.Printf("Error opening file: %v\n", err)
	}
	return file
}
